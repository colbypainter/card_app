class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:show, :edit, :update]
  before_action :admin_user,     only: [:destroy, :index]
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
    @card = current_user.cards.build if logged_in?
    @cards = @user.cards
  end
  
  def new
    @user = User.new 
  end
  
  def create 
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit 
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile Updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  ## This is used in the index by admin to destroy ANOTHER user
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  
  ## This is to destroy self
  ## Does not currently work
=begin 
  def destroy_self
    begin
      if destroy_stripe_self
        User.find_by_email(@current_user.email).destroy
        flash[:success] = "Your account has been successfully deleted."
        redirect_to root_url
      else 
        flash[:danger] = "Something went wrong, please try again."
        redirect_to request.referrer
      end
      
      rescue => e 
        flash[:danger] = e.message
    end
  end
=end
  
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, 
      :address_line1, :address_line2, :city, :state, :zip)
    end
    
    # Before filters

    
    # Confirms the correct user
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user
    def admin_user 
      redirect_to(root_url) unless current_user.admin?
    end
  
end
