class SubscriptionsController < ApplicationController
  
  def new
    @user = current_user
    render 'subscriptions/new'
  end
  
  def create
    
    begin
      @user = User.find_by_email(params[:stripeEmail])
      
      customer = Stripe::Customer.create(
        :email => @user.email,
        :card  => params[:stripeToken]
      )
      
      # Save customer ID to user
      @user.stripe_customer_token = customer.id
      @user.save
      
      flash[:success] = "Thanks for the info. Now go ahead and add some cards!"
      redirect_to @user
      
      rescue Stripe::StripeError => e
        flash[:danger] = e.message
        redirect_to request.referrer
      
      rescue => e 
        flash[:danger] = e.message
        redirect_to request.referrer
    end
  end
  
  def edit
    @user = current_user
    render 'subscriptions/edit'
  end
  
  def update
    
    begin
      @user = User.find_by_email(params[:stripeEmail])
      
      customer = Stripe::Customer.retrieve(@user.stripe_customer_token)
      customer.source = params[:stripeToken]
      customer.save
      
      flash[:success] = "Thanks for the info. Now go ahead and add some cards!"
      redirect_to @user
      
      rescue Stripe::StripeError => e
        flash[:danger] = e.message
        redirect_to request.referrer
      
      rescue => e 
        flash[:danger] = e.message
        redirect_to request.referrer
    end
    
  end
  
end
