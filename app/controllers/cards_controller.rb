class CardsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  
  def create
    # create the card object
    @card = current_user.cards.build(card_params)
    # see if card is valid with the model
    if @card.valid?
      # create the plan in stripe
      if create_plan(@card)
        # save to model
        if @card.save
          flash[:success] = "Success! You will begin receiving a card on this date 
            beginning this year. You will be charged on that date each year."
          redirect_to request.referrer
        else
          flash[:danger] = "Something went wrong. Please try again."
          redirect_to request.referrer
        end
      else 
        redirect_to request.referrer
      end
    else
      flash[:danger] = "Something went wrong! Enter both fields and try again.
      Required date format: yyyy-mm-dd"
      redirect_to request.referrer
    end
    
  end
  
  def destroy
    if destroy_plan(@card) && @card.destroy
      flash[:success] = "Card deleted. You will no longer receive this card or incur 
        any related charges."
      redirect_to request.referrer
    else
      flash[:danger] = "Oh no! Something went wrong! Please try again."
      redirect_to request.referrer
    end
  end
  
  
  private
  
    def card_params
      params.require(:card).permit(:type_of, :receive_by_date)
    end
    
    def correct_user
      @card = current_user.cards.find_by(id: params[:id])
      redirect_to root_url if @card.nil?
    end
  
end
