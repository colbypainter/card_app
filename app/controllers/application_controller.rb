class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  
  def create_plan(card)
    
    begin
      @user = User.find_by_email(@current_user.email)
      customer = Stripe::Customer.retrieve(@user.stripe_customer_token)
      start_date = DateTime.parse(@card.receive_by_date).to_i
      plan = customer.subscriptions.create({:plan => @card.type_of, :trial_end => start_date})
      @card.subscription_id = plan.id
      @card.save
      
      return true
      
      rescue Stripe::StripeError => e
        flash[:danger] = e.message
        return false
      
      rescue => e 
        flash[:danger] = e.message
        return false
    end
    
  end
  
  def destroy_plan(card)
    
    begin
      @user = User.find_by_email(@current_user.email)
      customer = Stripe::Customer.retrieve(@user.stripe_customer_token)
      customer.subscriptions.retrieve(@card.subscription_id).delete
      return true
      
      rescue Stripe::StripeError => e
        flash[:danger] = e.message
        return false
      
      rescue => e 
        flash[:danger] = e.message
        return false
    end
    
  end
  
=begin
  def destroy_stripe_self 
    ## Will destroy the current user in Stripe. Can't be used to delete another user through index
    ## DOES NOT CURRENTLY WORK
    begin
    
      @user = User.find_by_email(@current_user.email)
      customer = Stripe::Customer.retrieve(@user.stripe_customer_token)
      customer.delete
      return true
      
      rescue Stripe::StripeError => e
        flash[:danger] = e.message
        return false
      
      rescue => e 
        flash[:danger] = e.message
        return false
    end
  end
=end
  
  private
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
  
end
