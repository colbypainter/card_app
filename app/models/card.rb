class Card < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :type_of, presence: true
  DateRegex = /\A\d{4}-\d{2}-\d{2}\z/
  validates :receive_by_date, presence: true,
                              format: { with: DateRegex }
end
