class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, from: "noreply@earlycard.com", subject: "Earlycard account activation"
  end

  def password_reset(user)
    @user = user 
    mail to: user.email, from: "noreply@earlycard.com", subject: "Earlycard password reset"
  end
end
