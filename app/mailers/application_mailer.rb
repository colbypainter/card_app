class ApplicationMailer < ActionMailer::Base
  default from: "noreplay@earlycard.com"
  layout 'mailer'
end
