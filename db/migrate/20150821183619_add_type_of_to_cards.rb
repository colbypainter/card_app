class AddTypeOfToCards < ActiveRecord::Migration
  def change
    add_column :cards, :type_of, :text
  end
end
