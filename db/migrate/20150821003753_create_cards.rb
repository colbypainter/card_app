class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.text :type
      t.string :receive_by_date
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :cards, :users
    add_index :cards, [:user_id, :created_at]
  end
end
