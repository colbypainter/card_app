User.create!(name:  "Kt Painter",
             email: "ktpntr@email.com",
             password:              "password1",
             password_confirmation: "password1",
             address_line1: "1000 DMV Drive",
             city: "Richmond",
             state: "Virginia",
             zip: "23220",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

=begin
99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end
=end